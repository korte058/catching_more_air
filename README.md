
## Name
Catching_some_air

## Description
This script project was written to visualise and analyse the data used in the manuscript: Catching some air: A method to spatially quantify aerial triazole resistance in Aspergillus fumigatus.

In the R script we load and clean data the data from the international air sampling pilot, analyse it, generate figures of the sampled regions, the CFU totals and resistance fractions. The genotyping and phenotyping data of isolated resistant strains.


The following files are required to run this R script:
- RF_air_IP_cleaned.csv 
This fine contains total and resistance counts as well as metadat on samples from international air sampling pilot and includes the following variables:

Sample ID: an  arbitrary number given to the packages prior to them being handed out

Country: Country in which sample was taken

Region: Circular area with a 50 km radius within which the samples were clustered for analysis

City/Town: City/Town in which the sample was taken

Start date: date on which the trap was deployed and the stickers exposed to the air

End date: date on which the trap was taken down and the stickers were re-covered and no longer exposed to the air

Total.ITR: A. fumigatus CFU count in the permissive layer of the itraconazole-treated plate

Res.ITR: CFU count of colonies that had breached the surface of the itraconazole-treated layer after incubation and were visually (with the unaided eye) sporulating.

RF.ITR: The itraconazole (~4 mg/L) resistance fraction = Res.ITR/Total.ITR

Total.VOR: A. fumigatus CFU count in the permissive layer of the voriconazole-treated plate

Res.VOR: CFU count of colonies that had breached the surface of the voriconazole-treated layer after 
incubation and were visually (with the unaided eye) sporulating.

RF.VOR: The voriconazole (~2 mg/L) resistance fraction = Res.VOR/Total.VOR

Total control: CFU count on the untreated growth control plate

Date.Batch: The date on which proccessing of the sample was started. To be more specific, the date at which Flamingo medium was poured over the seals of the sample and incubation was started.

Note: note on the sample based on either information given the participant or observations in the lab. 

Exclude: Binary to quickly filter out samples that were considered unsuitable for further analysis either because low or high CFU counts. See manuscript for rationale. 

Lat: Latitude at the centre of the sampled region, does not relate to sample-specific location.

Long: Longitude at the centre of the sampled region, does not relate to sample-specific location.


- Weather_data_IP_study_nov_dec_jan22_23.csv : contains raw weather data of the sampled regions during the sampling interval of the pilot downloaded from: https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels-monthly-means?tab=overview (see link for full description of the data and the units). Contains the following variables:

Region: Circular area with a 50 km radius within which the samples were clustered for analysis

Wind Nov : 10 m Wind speed (m/S) for the month november 2022 This parameter is the horizontal speed of the wind, or movement of air, at a height of ten metres above the surface of the Earth. 

Wind Dec : 10 m Wind speed (m/S) for the month december 2022 This parameter is the horizontal speed of the wind, or movement of air, at a height of ten metres above the surface of the Earth.

Wind Jan : 10 m Wind speed (m/S) for the month januari 2023 This parameter is the horizontal speed of the wind, or movement of air, at a height of ten metres above the surface of the Earth.

UV Nov: UV radiation at the surface (J/m^2) for the month november 2022. This parameter is the amount of ultraviolet (UV) radiation reaching the surface. It is the amount of radiation passing through a horizontal plane.

UV Dec: UV radiation at the surface (J/m^2) for the month december 2022. This parameter is the amount of ultraviolet (UV) radiation reaching the surface. It is the amount of radiation passing through a horizontal plane.

UV Jan: UV radiation at the surface (J/m^2) for the month januari 2023. This parameter is the amount of ultraviolet (UV) radiation reaching the surface. It is the amount of radiation passing through a horizontal plane.

Temp Nov: Temperature (K) for the month november 2022. This parameter is the temperature of air at 2m above the surface of land, sea or inland waters. 2m temperature is calculated by interpolating between the lowest model level and the Earth's surface, taking account of the atmospheric conditions.

Temp Dec: Temperature (K) for the month december 2022. This parameter is the temperature of air at 2m above the surface of land, sea or inland waters. 2m temperature is calculated by interpolating between the lowest model level and the Earth's surface, taking account of the atmospheric conditions.

Temp Jan: Temperature (K) for the month januari 2023. This parameter is the temperature of air at 2m above the surface of land, sea or inland waters. 2m temperature is calculated by interpolating between the lowest model level and the Earth's surface, taking account of the atmospheric conditions.

Precipitation Nov: Total precipitation (m) for the month november 2022. This parameter is the accumulated liquid and frozen water, comprising rain and snow, that falls to the Earth's surface. It is the sum of large-scale precipitation and convective precipitation. 

Precipitation Dec: Total precipitation (m) for the month december 2022. This parameter is the accumulated liquid and frozen water, comprising rain and snow, that falls to the Earth's surface. It is the sum of large-scale precipitation and convective precipitation. 

Precipitation Jan: Total precipitation (m) for the month januari 2023. This parameter is the accumulated liquid and frozen water, comprising rain and snow, that falls to the Earth's surface. It is the sum of large-scale precipitation and convective precipitation. 

lat_rep: Latitude at the centre of the sampled region, does not relate to sample specific location.

lon_rep: Longitude at the centre of the sampled region, does not relate to sample specific location.

- Genotyping_IP_cleaned.csv : contains the TR-type genotypes of the isolated resistant strains
Contains the following variable:

Order: Ordering variable included in the file to readily be able to order the isolates by the order in which they were isolated. Contains the following variables:

Strain: Strain code with "I" for strains isolated from itraconazole and V for strains isolated from voriconazole followed by a numeber indicating the order in which they were isolated from the air sample plate. 

Air sample: The plate/air sample from which the isolate originates

Triazole: The triazole treatment the resistant strain grew on can be ITRA (itraconazole) or VORI (voriconazole)

Country: Country in which sample was taken

Region: Circular area with a 50 km radius within which the samples were clustered for analysis


## Project status
The manuscript has been published in AEM under the DOI: https://doi.org/10.1128/aem.00271-24